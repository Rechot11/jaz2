package org.apache.jsp;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;

public final class login_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

  private static final JspFactory _jspxFactory = JspFactory.getDefaultFactory();

  private static java.util.Vector _jspx_dependants;

  private org.glassfish.jsp.api.ResourceInjector _jspx_resourceInjector;

  public Object getDependants() {
    return _jspx_dependants;
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;

    try {
      response.setContentType("text/html; charset=UTF-8");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;
      _jspx_resourceInjector = (org.glassfish.jsp.api.ResourceInjector) application.getAttribute("com.sun.appserv.jsp.resource.injector");

      out.write("\r\n");
      out.write("<!DOCTYPE html PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\" \"http://www.w3.org/TR/html4/loose.dtd\">\r\n");
      out.write("<html>\r\n");
      out.write("<head>\r\n");
      out.write("<meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\">\r\n");
      out.write("<title>Insert title here</title>\r\n");
      out.write("</head>\r\n");
      out.write("\t<body>\r\n");
      out.write("\t\t<form action=\"login.jsp\" method=\"get\">\r\n");
      out.write("\t\t\t<label>Please log in</label><br>\r\n");
      out.write("\t\t\t<p>Login:</p>\r\n");
      out.write("\t\t\t<input type=\"text\" id=\"login\" name=\"login\"/>\r\n");
      out.write("\t\t\t<p>Password:</p>\r\n");
      out.write("\t\t\t<input type=\"text\" id=\"password\" NAME=\"password\"/><br>\r\n");
      out.write("\t\t\t<p>Email:</p>\r\n");
      out.write("\t\t\t<input type=\"text\" id=\"email\" NAME=\"email\"/><br>\r\n");
      out.write("\t\t\t\r\n");
      out.write("\t\t\r\n");
      out.write("\t\t\t\r\n");
      out.write("\t\t\t<p>Or create new a user</p>\r\n");
      out.write("\t\t\t<labeL>anonymous<input type=\"radio\" name=\"info\" value=\"anonymous\"/></label>\t\r\n");
      out.write("\t\t\t<labeL>common<input type=\"radio\" name=\"info\" value=\"common\"/></label>\r\n");
      out.write("\t\t\t<labeL>premium<input type=\"radio\" name=\"info\" value=\"premium\"/></label>\r\n");
      out.write("\t\t\t<labeL>admin<input type=\"radio\" name=\"info\" value=\"admin\"/></label>\r\n");
      out.write("\t\t\t\r\n");
      out.write("\t\t\t<p><a href=\"login\">Fill a form</a></p>\r\n");
      out.write("\t\t\t\r\n");
      out.write("\t\t\t<input type=\"submit\" value=\"submit\"/>\r\n");
      out.write("\t\t</form>\r\n");
      out.write("\t</body>\r\n");
      out.write("</html>");
    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          out.clearBuffer();
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
        else throw new ServletException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }
}
