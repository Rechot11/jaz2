package repositories;
import domain.Person;
import java.util.ArrayList;
import java.util.List;

public class DummyPersonRepository implements PersonRepository {

private static List<Person> db = new ArrayList<Person>();
public Person getPersonByEmailAddress(String email){
	for(Person person: db){
		if(person.getEmail().equalsIgnoreCase(email))
			return person;
	}
	return null;
}
public void add(Person person){
	db.add(person);
}
public int count(){
	return db.size();
}
}