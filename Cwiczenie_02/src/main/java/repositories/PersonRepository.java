package repositories;
import domain.Person;

public interface PersonRepository {
	Person getPersonByEmailAddress(String email);
void add(Person person);
int count();
}
