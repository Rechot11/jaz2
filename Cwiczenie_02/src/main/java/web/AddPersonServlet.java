package web;
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import domain.Person;
import repositories.DummyPersonRepository;
import repositories.PersonRepository;
@WebServlet("/add")
public class AddPersonServlet extends HttpServlet{
private static final long serialVisionUID = 1L;
protected void doGet(HttpServletRequest request, HttpServletResponse response)
throws ServletException, IOException{
	
	HttpSession session = request.getSession();
	//if(session.getAttribute("conf")!=null){
		//response.getWriter().println("Powtorne wypelnienie formularza zostalo zablokowane");
		//return;
	//}
	
	Person person = retrievePersonFromRequest(request);
	PersonRepository repository = new DummyPersonRepository();
	
	session.setAttribute("pers",person);
	
	repository.add(person);
	response.sendRedirect("success.jsp");
}
	
private Person retrievePersonFromRequest (HttpServletRequest request){
	Person result = new Person();
	result.setLogin(request.getParameter("login"));
	result.setPassword(request.getParameter("password"));
	result.setEmail(request.getParameter("email"));
	return result;
}
}